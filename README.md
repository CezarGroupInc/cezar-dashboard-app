**Installation guide**

This is a simple installation guide to get you started.

*We recommend that you read this before doing anything*

---

## Download the repository

You need to do a couple of things

1. **Clone** the repository.
2. Copy this command **git clone https://bitbucket.org/CezarGroupInc/cezar-dashboard-app.git**.
3. **Run** the command on your git bash.
4. And wait till is done

---

## Update the dependencies

With your git bash.

1. Type **Composer Update**
2. Wait for everything to Download
---

## Update the Environment

Just so you can run it you need to do this next steps.

1. On the root folder open your bash.
2. Type **cp .env.example .env**.
3. Type on your bash **php artisan key:generate**
4. Open the **.evn** file on your text editor
5. Change the mysql settings into your own database settings
